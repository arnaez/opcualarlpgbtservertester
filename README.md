# OpcUaLArlpGbtServerTester



## To run the server and tester images locally

To launch the quasar server from the image locally, one can do:
```
docker run -it --platform linux/amd64 -v /cvmfs:/cvmfs -p 49050:49050 -e CI_BUILDS_DIR=/cvmfs gitlab-registry.cern.ch/atlas-dcs-opcua-servers/opcualarlpgbtserver:opcualarlpgbtserver_tst_calibration_test_board_frmw_open62541 /bin/sh
${CI_BUILDS_DIR}/launchServer.sh connection_LAPP_casa.xml config_LAPP_casa.xml
```
where tst_calibration_test_board_frmw here is the name of the gitlab branch to use for the server image.
Note that this assumes that the connection_LAPP_casa.xml and config_LAPP_casa.xml files exist within /cvmfs (this can easily be adjusted by providing another CI_BUILDS_DIR variable, e.g. /LocalConfigFiles at LAPP where -v /home/atlaslar/OpcUaServerDev/LocalConfigFiles:/LocalConfigFiles).
Then, inside the quasar docker one can stop the running server then rerun it with the following in order to get its stdout:
```
cd build
./bin/OpcUaLArLpGBTServer --opcua_backend_config ./bin/ServerConfig.xml --config_file ./bin/config.xml
```

In order to run the tester image, one can do
```
docker run -it --platform linux/amd64 -v /cvmfs:/cvmfs -e CI_BUILDS_DIR=/cvmfs gitlab-registry.cern.ch/arnaez/opcualarlpgbtservertester/baseimageforquasar /bin/sh
```
then inside it, something like:
```
cd /cvmfs/opcualarlpgbtservertester/
export CI_BUILDS_DIR=$PWD
python test-scripts/testFirmware.py
```
Make sure that either the server DNS address points to the (V)M running the quasar server or change it in the test scripts file to point to it (most likely 'localhost' does the job if the two dockers run on the same machine).

## Add your files

