import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError

# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))
from LpGBT import *

serverAddress = "opc.tcp://server:49050"
whichLpGBT = "A"

if len(sys.argv)>1:
   serverAddress = sys.argv[1]
if len(sys.argv)>2:
   whichLpGBT = sys.argv[2]

def main():
  fResults = open("./testLpGBTcomm_cabane.log", "a")
  try:
    #Using high-level functions for accessing registers
    thisLpGBT = LpGBT(serverAddress,"ns=2;s=lapp_cabane.LpGBT_" + whichLpGBT)

    #CHIPCONFIG = 0x80
    thisLpGBT.registerWrite(0x0036, 0x80)

    #EPCLK1CHNCNTRH  = 0x070
    thisLpGBT.registerWrite(0x0070, 0x64)
    #EPCLK4CHNCNTRH  = 0x076
    thisLpGBT.registerWrite(0x0076, 0x64)
    #EPCLK5CHNCNTRH  = 0x078
    thisLpGBT.registerWrite(0x0078, 0x64)
    #EPCLK7CHNCNTRH  = 0x07c
    thisLpGBT.registerWrite(0x007c, 0x64)
    #EPCLK8CHNCNTRH  = 0x07e
    thisLpGBT.registerWrite(0x007e, 0x64)
    #EPCLK9CHNCNTRH  = 0x080
    thisLpGBT.registerWrite(0x0080, 0x64)
    #EPCLK10CHNCNTRH = 0x082
    thisLpGBT.registerWrite(0x0082, 0x64)
    #EPCLK13CHNCNTRH = 0x088
    thisLpGBT.registerWrite(0x0088, 0x64)
    #EPCLK14CHNCNTRH = 0x08a
    thisLpGBT.registerWrite(0x008a, 0x64)
    #EPCLK15CHNCNTRH = 0x08c
    thisLpGBT.registerWrite(0x008c, 0x64)
    #EPCLK16CHNCNTRH = 0x08e
    thisLpGBT.registerWrite(0x008e, 0x64)
    #EPCLK17CHNCNTRH = 0x090
    thisLpGBT.registerWrite(0x0090, 0x64)
    #EPCLK18CHNCNTRH = 0x092
    thisLpGBT.registerWrite(0x0092, 0x64)
    #EPCLK19CHNCNTRH = 0x094
    thisLpGBT.registerWrite(0x0094, 0x64)
    #EPCLK22CHNCNTRH = 0x09a
    thisLpGBT.registerWrite(0x009a, 0x64)
    #EPCLK23CHNCNTRH = 0x09c
    thisLpGBT.registerWrite(0x009c, 0x64)

    #EPTXDATARATE = 0xff
    thisLpGBT.registerWrite(0x00A8, 0xff)

    #EPTXCONTROL = 0x0f
    thisLpGBT.registerWrite(0x00A9, 0x0f)

    #EPTX10ENABLE = 0xff
    thisLpGBT.registerWrite(0x00AA, 0xff)

    #EPTX32ENABLE = 0xab
    thisLpGBT.registerWrite(0x00AB, 0xff)

    #EPTX00CHNCNTR = 0xae
    thisLpGBT.registerWrite(0x00ae, 0x03)
    #EPTX01CHNCNTR = 0xaf
    thisLpGBT.registerWrite(0x00af, 0x03)
    #EPTX02ChnCntr = 0xb0
    thisLpGBT.registerWrite(0x00b0, 0x03)
    #EPTX03ChnCntr = 0xb1
    thisLpGBT.registerWrite(0x00b1, 0x03)
    #EPTX10CHNCNTR = 0xb2 
    thisLpGBT.registerWrite(0x00B2, 0x03)
    #EPTX11ChnCntr = 0xb3
    thisLpGBT.registerWrite(0x00b3, 0x03)
    #EPTX12ChnCntr = 0xb4
    thisLpGBT.registerWrite(0x00b4, 0x03)
    #EPTX13CHNCNTR = 0xb5
    thisLpGBT.registerWrite(0x00b5, 0x03)
    #EPTX20CHNCNTR = 0xb6
    thisLpGBT.registerWrite(0x00b6, 0x03)
    #EPTX21CHNCNTR = 0xb7
    thisLpGBT.registerWrite(0x00b7, 0x03)
    #EPTX22CHNCNTR = 0xb8
    thisLpGBT.registerWrite(0x00b8, 0x03)
    #EPTX23CHNCNTR = 0xb9
    thisLpGBT.registerWrite(0x00b9, 0x03)
    #EPTX30CHNCNTR = 0xba
    thisLpGBT.registerWrite(0x00ba, 0x03)
    #EPTX31CHNCNTR = 0xbb
    thisLpGBT.registerWrite(0x00bb, 0x03)
    #EPTX32CHNCNTR = 0xbc
    thisLpGBT.registerWrite(0x00bc, 0x03)
    #EPTX33CHNCNTR = 0xbd
    thisLpGBT.registerWrite(0x00bd, 0x03)


    #ULDATASOURCE5 = 0x0
    thisLpGBT.registerWrite(0x012D, 0x0)
    ##PS2CONFIG = 0x64
    #thisLpGBT.registerWrite(0x0063, 0x24)
    #TO0SEL = 0x143
    thisLpGBT.registerWrite(0x0143, 0x2)
    #POWERUP2 = 0x6
    thisLpGBT.registerWrite(0x00FB, 0x6)

    print("Checking lpGBT " + str(whichLpGBT) + " main register values:")
    regs = [ 0x0036, 0x76, 0xa8, 0xa9, 0xaa, 0xb2, 0xc0, 0x12d, 0x63, 0xfb, 0x100, 0x107, 0x10e, 0x16f, 0x184, 0x199]
    for reg in regs:
       print("lpGBT register " + str(reg) + " :")
       val = thisLpGBT.registerRead(reg)
       print("                                " + str(val))
       fResults.write("LpGBT_" + whichLpGBT + ".registerRead" + str(reg) + "=\"" + str(val[1]) +"\"\n")

    #Reset GPIOs
    #time.sleep(2)
    #thisLpGBT.registerWrite(0x54, 0x0)
    #thisLpGBT.registerWrite(0x5c, 0x0)
    #thisLpGBT.registerWrite(0x5a, 0x0)
    #thisLpGBT.registerWrite(0x58, 0x0)
    #thisLpGBT.registerWrite(0x53, 0x0)
    #thisLpGBT.registerWrite(0x5b, 0x0)
    #thisLpGBT.registerWrite(0x59, 0x0)
    #thisLpGBT.registerWrite(0x57, 0x0)

    thisLpGBT.disconnect()


    # Closing results file
    fResults.close()
    os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
