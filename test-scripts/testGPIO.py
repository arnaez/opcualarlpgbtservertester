import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError

ladoc_addr=0

# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))

from DigitalIO import *

######### GPIO config
#print("GPIO config")
##INFO:drivers.lpgbt.ipbus_lpgbt_interface:w [64] at 84=0x54
#LpGBT0.registerWrite(0x0054, 64)
##INFO:drivers.lpgbt.ipbus_lpgbt_interface:w [0] at 83=0x53
#LpGBT0.registerWrite(0x0053, 0x0)
##INFO:drivers.lpgbt.ipbus_lpgbt_interface:w [64] at 86=0x56
#LpGBT0.registerWrite(0x0056, 64)
##INFO:drivers.lpgbt.ipbus_lpgbt_interface:w [0] at 85=0x55
#LpGBT0.registerWrite(0x0055, 0x0)
#time.sleep(2)




def main():
  fResults = open("./testGPIO.log", "a")
  res = 0
  try:
    gpio_6 = DigitalIO("opc.tcp://server:49050","ns=2;s=latournett_emf.LpGBT0.GPIO.test")
    gpio_6.configure()
    print("Writin 1 on GPIO 6")
    gpio_6.write(1)

    time.sleep(1)
    print("Writin 0 on GPIO 6")
    gpio_6.write(0)
    val = gpio_6.read()
    fResults.write("LpGBT0__GPIO__test.GPIOoff=" + str(val[1]) + "\n")
    
    time.sleep(1)
    print("Writin 1 on GPIO 6")
    gpio_6.write(1)
    val = gpio_6.read()
    fResults.write("LpGBT0__GPIO__test.GPIOon=" + str(val[1]) + "\n")

    gpio_6.disconnect()
    res = 1
  except Exception as e:
    print(f"An error occurred:", e)
    os._exit(os.EX_OK)

  # Closing results file
  fResults.close()
  os._exit(os.EX_OK)

    
if __name__ == "__main__":
  main()
