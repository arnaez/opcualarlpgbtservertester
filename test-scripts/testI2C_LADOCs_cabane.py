import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError


# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))

from I2CMaster import *
from I2CSlaveLADOC import *
from LpGBT import *

serverAddress = "opc.tcp://server:49050"
whichLpGBT = "A"
whichMaster = "M0"

if len(sys.argv)>1:
   serverAddress = sys.argv[1]
if len(sys.argv)>2:
   whichLpGBT = sys.argv[2]
if len(sys.argv)>3:
   whichMaster = sys.argv[3]

def read_hidden_register(client, addr):
        res = None
        try:
                print("  Reading hidden register #" + str(addr) + "...")
                res = client.readSlaveHiddenReg(addr)
                print("    read:", res)
        except Exception as inst:
                print("  Well, issue when getting I2C hidden reg read output...")
                print(type(inst))    # the exception type
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly
        return res

def read_hidden_registers(client):
	print(" In read_hidden_registers(client)")
	reg_map = {}
	for reg in range(9):
		reg_map[reg] = read_hidden_register(client, reg)
	return reg_map

def write_hidden_register(client, reg_address, reg_value):
        """ Write casa register using indirect addressing via i2c
        Arguments:
        reg_addr: indirect address (between 0 and 8 included)
        """
        print("   In write_hidden_register(" + str(reg_address) + ")")

        result_variants=None
        try:
                result_variants = client.writeSlaveHiddenReg(reg_address, [reg_value])
                print("Call I2C hidden reg write result:", result_variants) #should be an array of Bytes
        except Exception as inst:
                print("Well, issue when getting I2C hidden reg write output but continuing...")
                print(type(inst))    # the exception type
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly

                pass

        return result_variants

def write_registers(client, cfg_ladoc):
        for addr in cfg_ladoc:
            print("  trying to read hidden register #" + str(addr))
            write_hidden_register(client, int(addr), cfg_ladoc[addr])
            read_hidden_register(client, int(addr))


def main():
  fResults = open("./testI2C_LADOCs_cabane.log", "a")
  try:

    thisLpGBT = LpGBT(serverAddress,"ns=2;s=lapp_cabane.LpGBT_" + whichLpGBT)
    thisLpGBT.configure()

    # RST0
    thisLpGBT.registerWrite(0x013c, 0x0)
    thisLpGBT.registerWrite(0x013c, 0x7)
    thisLpGBT.registerWrite(0x013c, 0x0)


    # Configuring the I2C master 0
    master_I2C = I2CMaster(serverAddress,"ns=2;s=lapp_cabane.LpGBT_" + whichLpGBT + ".I2C" + whichMaster)
    print("Configuring cabane master " + whichMaster + " I2C for LpGBT " + whichLpGBT)
    try:
        master_I2C.configure(nbWords=1)
        print("I2C Master config done")
    except Exception as inst:
        print("Well, issue when setting I2C master configuration...")
        print(type(inst))    # the exception type
        print(inst.args)     # arguments stored in .args
        print(inst)          # __str__ allows args to be printed directly
        pass
    print("Configuring cabane LADOC slaves I2C on LpGBT " + whichLpGBT + " Master " + whichMaster)

    # Instanciating LADOC objects
    ladocs = {}
    nbMaxLadocs = 6
    if whichMaster=="M2":
        nbMaxLadocs = 4
    for iLadoc in range(nbMaxLadocs):
        ladocs[str(iLadoc)] =  I2CSlaveLADOC(serverAddress,"ns=2;s=lapp_cabane.LpGBT_" + whichLpGBT + ".I2C" + whichMaster + ".ladoc" + str(iLadoc+1))
        # Now testing the communication with the LADOC
        print("pinging LADOC #" + str(iLadoc+1))
        # First readint out a main register
        try:
            print("Reading R7")
            result_variants = ladocs[str(iLadoc)].readSlaveMainReg(0x7)
            print("Call I2C R7 read result:", result_variants) #should be an array of Bytes
        except Exception as inst:
            print("Well, issue when getting I2C R7 read output but continuing...")
            print(type(inst))    # the exception type
            print(inst.args)     # arguments stored in .args
            print(inst)          # __str__ allows args to be printed directly

    

    # Trying to write some LADOC registers
    print("Trying to write LADOC registers")
    ladoc_cfg = {}
    ladoc_cfg['data_inenable'] = {}
    ladoc_cfg['data_inenable']['0'] = 0x0   # DAC LSB
    ladoc_cfg['data_inenable']['1'] = 0x0   # DAC MSB
    ladoc_cfg['data_inenable']['2'] = 217   # fine tuning
    ladoc_cfg['data_inenable']['3'] = 0xff  # all mirrors enabled
    ladoc_cfg['data_inenable']['4'] = 0x1c  # OTA in and out ON, CLAROC channel 1 enabled
    ladoc_cfg['data_inenable']['5'] = 0x0   # no cmd pulse channel enabled
    ladoc_cfg['data_inenable']['6'] = 127   # coarse tuning + others
    ladoc_cfg['data_inenable']['7'] = 0x0   # CLPS driver disabled
    ladoc_cfg['data_inenable']['8'] = 0x0   # no outputs enabled
    print(ladocs)
    for iLadoc, ladoc in ladocs.items():
        print("Configuring LADOC #" + str(int(iLadoc)+1))
        try:
            write_registers(ladoc, ladoc_cfg['data_inenable'])
        except Exception as inst:
            print("Well, issue when configuring the LADOC but continuing...")
            print(type(inst))    # the exception type
            print(inst.args)     # arguments stored in .args
            print(inst)          # __str__ allows args to be printed directly


    #Reading back registers values
    for iLadoc, ladoc in ladocs.items():
        print("Trying to read LADOC #" + str(int(iLadoc)+1) + " hidden registers after configuration")
        regs_values = {}
        regs_values = read_hidden_registers(ladoc)
        print(regs_values)
        for reg,val in regs_values.items():
            print(val)
            fResults.write("LpGBT_" + whichLpGBT + "__I2CM0__ladoc" + str(int(iLadoc)+1) + ".hiddenReg" + str(reg) + "=\"" + str(val[1][0]) + "\"\n") 


    # Closing results file
    fResults.close()
    for iLadoc, ladoc in ladocs.items():
        ladoc.disconnect()
    master_I2C.disconnect()
    os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
