import sys
import xml.etree.ElementTree as ET

def parse_log_file(log_file_path):
    properties = {}
    with open(log_file_path, 'r') as file:
        for line in file:
            if '=' in line:
                key, value = line.strip().split('=', 1)
                properties[key.strip()] = value.strip()
    return properties

def generate_junit_xml(testsuitename, testcasename, properties, junit_xml_path):
    testsuite = ET.Element('testsuite', name=testsuitename, tests='1')
    testcase = ET.SubElement(testsuite, 'testcase', classname='Test', name=testcasename)

    for key, value in properties.items():
        ET.SubElement(testcase, 'property', name=key, value=value)

    tree = ET.ElementTree(testsuite)
    tree.write(junit_xml_path, xml_declaration=True, encoding='utf-8')

if __name__ == "__main__":
    print("Inside produceJUnitXML.py")
    if (len(sys.argv)>1):
        print  ("Provided ", str(len(sys.argv)-1), " ARGs",sys.argv[1:])
    else:
        print("No argument provided!")
    if len(sys.argv) != 4:
        print("Usage: python produceJUnitXML.py <testcasename> <input_log_path> <output_junit_path>")
        sys.exit(1)

    testcasename = sys.argv[1]
    log_file_path = sys.argv[2]
    junit_xml_path = sys.argv[3]

    print("Parsing")
    properties = parse_log_file(log_file_path)
    print("Generating JUnit file")
    generate_junit_xml('TestSuite', testcasename, properties, junit_xml_path)
    print("JUnit file generated at " + junit_xml_path)
