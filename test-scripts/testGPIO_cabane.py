import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError


# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))

from DigitalIO import *
from LpGBT import *


serverAddress = "opc.tcp://server:49050"
whichLpGBT = "A"

if len(sys.argv)>1:
   serverAddress = sys.argv[1]
if len(sys.argv)>2:
   whichLpGBT = sys.argv[2]


def main():
  fResults = open("./testGPIO_cabane.log", "a")
  res = 0
  try:
    testGPIOs={8, 15, 1, 3, 5, 12, 9, 10}
    for iGPIO in testGPIOs:
       gpio = DigitalIO(serverAddress,"ns=2;s=lapp_cabane.LpGBT_" + whichLpGBT + ".GPIO.gpio" + str(iGPIO))
       gpio.configure()
       gpio.write(1)
       time.sleep(0.5)
       val = gpio.read()
       fResults.write("LpGBT_" + whichLpGBT + "__GPIO__gpio" + str(iGPIO) + ".on=" + str(val[1]) + "\n")
       gpio.write(0)
       time.sleep(0.5)
       val = gpio.read()
       fResults.write("LpGBT_" + whichLpGBT + "__GPIO__gpio" + str(iGPIO) + ".off=" + str(val[1]) + "\n")
       gpio.write(1)
       gpio.disconnect()

    ##Using high-level functions for accessing registers
    #LpGBT0 = LpGBT("opc.tcp://server:49050","ns=2;s=latournett_emf.LpGBT0")

    ##CHIPCONFIG = 0x80
    #LpGBT0.registerWrite(0x0055, 0x7f)
    #time.sleep(0.01)
    #LpGBT0.registerWrite(0x0055, 0xff)
    #LpGBT0.disconnect()


    res = 1
  except Exception as e:
    print(f"An error occurred:", e)
    os._exit(os.EX_OK)

  # Closing results file
  fResults.close()
  os._exit(os.EX_OK)

    
if __name__ == "__main__":
  main()
