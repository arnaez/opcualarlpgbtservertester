import pytest
import os



def convert_value(value):
    try:
        # Attempt to convert to int
        return int(value)
    except ValueError:
        try:
            # Attempt to convert to float
            return float(value)
        except ValueError:
            # If unable to convert, return as is
            return value

def read_properties(file_path):
    properties = {}
    with open(file_path, 'r') as file:
        for line in file:
            key, value = line.strip().split('=')
            properties[key] = value
    return properties


def test_compare_properties(testcases):
    log_file_path = testcases[0]
    ref_file_path = testcases[1]

    log_properties = read_properties(log_file_path)
    ref_properties = read_properties(ref_file_path)

    testcase = testcases[2]
    subtest = testcases[3]
    key = f"{testcase}.{subtest}"
    log_value = convert_value(log_properties.get(key, None))
    ref_value = convert_value(ref_properties.get(key, None))

    assert log_value == ref_value, f"Property {key} mismatch. Expected: {ref_value} ({type(ref_value)}), Got: {log_value} ({type(log_value)})"

