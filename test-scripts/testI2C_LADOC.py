import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError


# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))

from I2CMaster import *
from I2CSlaveLADOC import *


def read_hidden_register(client, addr):
        res = None
        try:
                print("  Reading hidden register #" + str(addr) + "...")
                res = client.readSlaveHiddenReg(addr)
                print("    read:", res)
        except Exception as inst:
                print("  Well, issue when getting I2C hidden reg read output...")
                print(type(inst))    # the exception type
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly
        return res

def read_hidden_registers(client):
	print(" In read_hidden_registers(client)")
	reg_map = {}
	for reg in range(9):
		reg_map[reg] = read_hidden_register(client, reg)
	return reg_map

def write_hidden_register(client, reg_address, reg_value):
        """ Write casa register using indirect addressing via i2c
        Arguments:
        reg_addr: indirect address (between 0 and 8 included)
        """
        print("   In write_hidden_register(" + str(reg_address) + ")")

        result_variants=None
        try:
                result_variants = client.writeSlaveHiddenReg(reg_address, [reg_value])
                print("Call I2C hidden reg write result:", result_variants) #should be an array of Bytes
        except Exception as inst:
                print("Well, issue when getting I2C hidden reg write output but continuing...")
                print(type(inst))    # the exception type
                print(inst.args)     # arguments stored in .args
                print(inst)          # __str__ allows args to be printed directly

                pass

        return result_variants

def write_registers(client, cfg_ladoc):
        for addr in cfg_ladoc:
            print("  trying to read hidden register #" + str(addr))
            write_hidden_register(client, int(addr), cfg_ladoc[addr])
            read_hidden_register(client, int(addr))


def main():
  fResults = open("./testI2C_LADOC.log", "a")
  try:
    #fResults.write("LpGBT0.registerRead54=\"" + str(result_variants) +"\"\n")

    # Configuring the I2C master 0
    master_casa = I2CMaster("opc.tcp://server:49050","ns=2;s=latournett_emf.LpGBT0.casa")
    print("Configuring casa master I2C")
    try:
        master_casa.configure(nbWords=1)
        print("I2C Master config done")
    except Exception as inst:
        print("Well, issue when setting I2C master configuration...")
        print(type(inst))    # the exception type
        print(inst.args)     # arguments stored in .args
        print(inst)          # __str__ allows args to be printed directly
        pass

    print("Configuring casa LADOC slave I2C")
    ladoc0 = None
    try:
        ladoc0 = I2CSlaveLADOC("opc.tcp://server:49050","ns=2;s=latournett_emf.LpGBT0.casa.ladoc0")
        print("I2C Master config done")
    except Exception as inst:
        print("Well, issue when instantiating I2C LADOC slave...")
        print(type(inst))    # the exception type
        print(inst.args)     # arguments stored in .args
        print(inst)          # __str__ allows args to be printed directly
        pass


        
    # Now testing the communication with the LADOC
    # First readint out a main register
    try:
        print("Reading R7")
        result_variants = ladoc0.readSlaveMainReg(0x7)
        print("Call I2C R7 read result:", result_variants) #should be an array of Bytes
    except Exception as inst:
        print("Well, issue when getting I2C R7 read output but continuing...")
        print(type(inst))    # the exception type
        print(inst.args)     # arguments stored in .args
        print(inst)          # __str__ allows args to be printed directly

    
    print("Trying to read LADOC hidden registers")
    regs_values = {}
    try:
        regs_values = read_hidden_registers(ladoc0)
    except:
        print(f"An error occured with I2C read test")
    print("Done with I2C read test")



    # Trying to write some LADOC registers
    print("Trying to write LADOC registers")
    ladoc_cfg = {}
    ladoc_cfg['data_inenable'] = {}
    ladoc_cfg['data_inenable']['0'] = 0x0   # DAC LSB
    ladoc_cfg['data_inenable']['1'] = 0x0   # DAC MSB
    ladoc_cfg['data_inenable']['2'] = 217   # fine tuning
    ladoc_cfg['data_inenable']['3'] = 0xff  # all mirrors enabled
    ladoc_cfg['data_inenable']['4'] = 0x1c  # OTA in and out ON, CLAROC channel 1 enabled
    ladoc_cfg['data_inenable']['5'] = 0x0   # no cmd pulse channel enabled
    ladoc_cfg['data_inenable']['6'] = 127   # coarse tuning + others
    ladoc_cfg['data_inenable']['7'] = 0x0   # CLPS driver disabled
    ladoc_cfg['data_inenable']['8'] = 0x0   # no outputs enabled
    write_registers(ladoc0, ladoc_cfg['data_inenable'])

    #Reading back registers values
    print("Reading back values after config")
    regs_values = read_hidden_registers(ladoc0)
    print(regs_values)
    for reg,val in regs_values.items():
        print(val)
        fResults.write("LpGBT0__casa__ladoc0.hiddenReg" + str(reg) + "=\"" + str(val[1][0]) + "\"\n") 


    # Closing results file
    fResults.close()
    ladoc0.disconnect()
    master_casa.disconnect()
    os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
