import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError

# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))
from LpGBT import *

def main():
  fResults = open("./testLpGBTcomm.log", "a")
  try:
    #Using high-level functions for accessing registers
    LpGBT0 = LpGBT("opc.tcp://server:49050","ns=2;s=latournett_emf.LpGBT0")
    LpGBT0.configure()
    #CHIPCONFIG = 0x80
    LpGBT0.registerWrite(0x0036, 0x80)
    #EPCLK4CHNCNTRH = 0x24
    LpGBT0.registerWrite(0x0076, 0x24)
    #EPTXDATARATE = 0x0c
    LpGBT0.registerWrite(0x00A8, 0x0c)
    #EPTXCONTROL = 0x0
    LpGBT0.registerWrite(0x00A9, 0x0)
    #EPTX10ENABLE = 0x10
    LpGBT0.registerWrite(0x00AA, 0x10)
    #EPTX10CHNCNTR = 0x03 
    LpGBT0.registerWrite(0x00B2, 0x03)
    #EPTX11_10CHNCNTR = 0x0
    LpGBT0.registerWrite(0x00C0, 0x0)
    #ULDATASOURCE5 = 0x0
    LpGBT0.registerWrite(0x012D, 0x0)
    #PS2CONFIG = 0x64
    LpGBT0.registerWrite(0x0063, 0x24)
    #POWERUP2 = 0x6
    LpGBT0.registerWrite(0x00FB, 0x6)

    print("Checking main lpGBT register values:")
    regs = [ 0x0036, 0x76, 0xa8, 0xa9, 0xaa, 0xb2, 0xc0, 0x12d, 0x63, 0xfb ]
    for reg in regs:
       print("lpGBT register " + str(reg) + " :")
       val = LpGBT0.registerRead(reg)
       print("                                " + str(val))
       fResults.write("LpGBT0.registerRead" + str(reg) + "=\"" + str(val[1]) +"\"\n")

    LpGBT0.disconnect()

    #Low-level style for accessing registers
    print("Setting OPC-UA server")
    client = Client("opc.tcp://server:49050")
    print("Connecting to OPC-UA server")
    client.connect()
    print("...connected")
    client.load_type_definitions()
    print("...client set.")

    # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
    print("Getting root node")
    root = client.get_root_node()

    # Setting lpgbt communication nodes
    lpgbt         = client.get_node("ns=2;s=latournett_emf.LpGBT0")
    registerRead_node  = client.get_node("ns=2;s=latournett_emf.LpGBT0.registerRead")
    registerWrite_node = client.get_node("ns=2;s=latournett_emf.LpGBT0.registerWrite")

    args = (ua.Variant(0x0036, ua.VariantType.UInt16), ua.Variant(0x80, ua.VariantType.Byte))
    result_variants=None
    try:
      result_variants = lpgbt.call_method(registerWrite_node, *args)
      print("Call 2")
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(1)

    # Trying to read out register #54 (0x36)
    args = (ua.Variant(54, ua.VariantType.UInt16),)
    result_variants=None
    try:
      result_variants = lpgbt.call_method(registerRead_node, *args)
      print("Call 1 result:", result_variants)
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(1)
    fResults.write("LpGBT0.registerRead54=\"" + str(result_variants) +"\"\n")

    # Trying to write in register #257 (0x101) value 69
    args = (ua.Variant(257, ua.VariantType.UInt16), ua.Variant(69, ua.VariantType.Byte))
    result_variants=None
    try:
      result_variants = lpgbt.call_method(registerWrite_node, *args)
      print("Call 2")
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(1)
    # Reading back value
    args = (ua.Variant(257, ua.VariantType.UInt16),)
    result_variants=None
    try:
      result_variants = lpgbt.call_method(registerRead_node, *args)
      print("Call 3 result:", result_variants)
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(1)
    fResults.write("LpGBT0.registerWrite257=\"" + str(result_variants) +"\"\n")
    # Writing back 0 as value
    args = (ua.Variant(257, ua.VariantType.UInt16), ua.Variant(0, ua.VariantType.Byte))
    result_variants=None
    try:
      result_variants = lpgbt.call_method(registerWrite_node, *args)
      print("Call 4")
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(5)


    # Closing results file
    fResults.close()
    os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
