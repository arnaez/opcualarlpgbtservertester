from opcua import Client, ua

def get_method_signature(method_node):
    """
    Get the input and output arguments of an OPC UA method by browsing its children.
    """
    input_args = []
    output_args = []

    try:
        # Browse children of the method node to find InputArguments and OutputArguments
        children = method_node.get_children()
        for child in children:
            child_browse_name = child.get_browse_name().Name

            # Check if the child is InputArguments or OutputArguments
            if child_browse_name == "InputArguments":
                input_arguments = child.get_value()
                for arg in input_arguments:
                    input_args.append({
                        'Name': arg.Name,
                        'DataType': arg.DataType,
                        'Description': arg.Description.Text
                    })
            elif child_browse_name == "OutputArguments":
                output_arguments = child.get_value()
                for arg in output_arguments:
                    output_args.append({
                        'Name': arg.Name,
                        'DataType': arg.DataType,
                        'Description': arg.Description.Text
                    })

    except Exception as e:
        print(f"Failed to retrieve method signature for {method_node}: {e}")

    return input_args, output_args

def list_methods_on_all_nodes_with_signatures(opcua_url):
    # Create an OPC UA client and connect to the server
    client = Client(opcua_url)
    try:
        client.connect()

        # Get the root node
        root = client.get_root_node()
        
        # Recursive function to browse and list methods
        def browse_and_list_methods(node):
            methods = []
            # Get children of the current node
            children = node.get_children()
            for child in children:
                # Get the node's attributes
                node_class = child.get_node_class()
                
                # If the node is of type 'Method'
                if node_class == ua.NodeClass.Method:
                    methods.append(child)
                
                # Recursively browse the children
                methods.extend(browse_and_list_methods(child))
            return methods

        # Start browsing from the root node
        all_methods = browse_and_list_methods(root)

        # Print all methods and their signatures
        for method in all_methods:
            print(f"Method found: {method.get_display_name().Text} (Node ID: {method.nodeid})")
            input_args, output_args = get_method_signature(method)
            
            print("  Input Arguments:")
            for arg in input_args:
                print(f"    - Name: {arg['Name']}, DataType: {arg['DataType']}, Description: {arg['Description']}")
            
            print("  Output Arguments:")
            for arg in output_args:
                print(f"    - Name: {arg['Name']}, DataType: {arg['DataType']}, Description: {arg['Description']}")

    except Exception as e:
        print(f"Error: {e}")
    finally:
        client.disconnect()


def call_opcua_method(opcua_url, method_node_id, parent_node_id, address, data):
    client = Client(opcua_url)
    try:
        client.connect()

        # Retrieve the parent node
        parent_node = client.get_node(parent_node_id_str)
        
        # Retrieve the method node
        method_node = client.get_node(method_node_id_str)

        # Convert the arguments to UInt16 and wrap them in a Variant array
        #address_variant = ua.Variant(ua.TwoByteNodeId(address), ua.TwoByteNodeId)
        #data_variant = ua.Variant(ua.TwoByteNodeId(data), ua.TwoByteNodeId)

        # Call the method with the wrapped arguments
        #result = parent_node.call_method(method_node, address_variant, data_variant)
        args = (ua.Variant(address, ua.VariantType.Byte),ua.Variant([data], ua.VariantType.Byte),)
        result = parent_node.call_method(method_node, *args)

        # Print the result
        print(f"Method call result: {result}")

    except Exception as e:
        print(f"Error occurred: {e}")
    finally:
        client.disconnect()


# Example usage
opcua_url = "opc.tcp://server:49050"  # Replace with your OPC UA server URL
list_methods_on_all_nodes_with_signatures(opcua_url)

#method_node_id_str = "ns=2;s=latournett_emf.LpGBT0.casa.ladoc0.writeSlaveMainReg"  # Method Node ID
#parent_node_id_str = "ns=2;s=latournett_emf.LpGBT0.casa.ladoc0"  # Parent node ID of the method
#address = 2  # Example address
#data = 3  # Example data

#call_opcua_method(opcua_url, method_node_id_str, parent_node_id_str, address, data)



