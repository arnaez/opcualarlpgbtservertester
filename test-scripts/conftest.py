# conftest.py
import pytest

def pytest_addoption(parser):
    parser.addoption("--logfilename", action="store", default="log.txt")
    parser.addoption("--logfileref", action="store", default="log.ref")
    parser.addoption("--suitename", action="store", default="pypytest")

def read_properties(file_path):
    properties = {}
    with open(file_path, 'r') as file:
        for line in file:
            key, value = line.strip().split('=')
            properties[key] = value
    return properties

def pytest_configure(config):
    #if config.option.site == 'foo':
        config.inicfg['junit_suite_name'] = config.option.suitename

@pytest.fixture(autouse=True)
def record_index(record_xml_attribute, pytestconfig):
    record_xml_attribute('classname', pytestconfig.option.suitename)

def pytest_generate_tests(metafunc):
    log_properties = read_properties(metafunc.config.getoption("logfilename"))
    ref_properties = read_properties(metafunc.config.getoption("logfileref"))

    testcases = []
    idlist = []

    # Collect all testcases from both log and reference properties
    for key in log_properties.keys() | ref_properties.keys():
        testcase, subtest = key.split('.')
        testcases.append((metafunc.config.getoption("logfilename"), metafunc.config.getoption("logfileref"), testcase, subtest))
        idlist.append(str(key))

    metafunc.parametrize("testcases", testcases, ids=idlist)
