import os
import sys
import time
import logging

from opcua import Client
from asyncua.ua.uaerrors import UaStatusCodeError

serverAddress = "opc.tcp://server:49050"
if len(sys.argv)>1:
   serverAddress = sys.argv[1]


def main():
  fResults = open("./testFirmware_cabane.log", "a")
  try:
    print("Setting OPC-UA server")
    client = Client(serverAddress)
    print("Connecting to OPC-UA server " + serverAddress)
    client.connect()
    print("...connected")
    client.load_type_definitions()
    print("...client set.")

    # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects
    print("Getting root node")
    root = client.get_root_node()


    update_node   = client.get_node("ns=2;s=lapp_cabane.Monitoring.update")
    firmware_name = client.get_node("ns=2;s=lapp_cabane.Monitoring.firmware_name")
    monitoring    = client.get_node("ns=2;s=lapp_cabane.Monitoring")

    try:
      print("Call result:",monitoring.call_method(update_node))
    except:
      print("Well, issue when updating the monitoring node but continuing...")
      pass
    time.sleep(1)

    #print(firmware_name.get_data_value())
    ret = firmware_name.get_data_value()
    print(ret)
    print("Value"    ,ret.Value.Value)
    print("Type"     ,ret.Value.VariantType)
    print("Status"   ,ret.StatusCode)
    print("Timestamp",ret.SourceTimestamp)

    fResults.write("Monitoring.firmware_name=\"" + str(ret.Value.Value) +"\"\n")
    fResults.close()
    if ret.Value.Value == 'calibration_test':
      print("Exiting without problem")
      os._exit(os.EX_OK)
    else:
      print("Wrong obtained valued.")
      os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
