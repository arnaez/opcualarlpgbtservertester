import os
import sys
import time
import logging

from opcua import Client
from opcua import ua
from asyncua.ua.uaerrors import UaStatusCodeError

# adding Folder_2/subfolder to the system path
sys.path.append(os.path.join(os.path.dirname(sys.path[0]), 'PyUaoForQuasar', 'generated'))
from LpGBT import *

sys.path.append("/home/atlaslar/CodeLucas/python-testbench-atlas-calibration-ladoc_claroc/drivers/lpgbt/")
from lpgbt_register_map_v1 import *

def main():
  fResults = open("./dumpLpGBTstate.log", "a")
  try:
    #Using high-level functions for accessing registers
    LpGBT_A = LpGBT("opc.tcp://server:49050","ns=2;s=lapp_cabane.LpGBT_A")

    print("Checking main lpGBT register values:")
    for reg in LpgbtRegisterMapV1.Reg:
       print("lpGBT register " + str(reg.name) + " :")
       val = LpGBT_A.registerRead(int(reg.value))
       print("                                " + str(val))
       fResults.write(str(reg.name) + "=\"" + str(val[1]) +"\"\n")

    LpGBT_A.disconnect()

    # Closing results file
    fResults.close()
    os._exit(os.EX_OK)
  except:
    print(f"An error occurred")
    os._exit(os.EX_OK)
    
if __name__ == "__main__":
  main()
