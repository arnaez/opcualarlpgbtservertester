Instructions to reproduce/update the base image in which the (client) tests are run

Go to this directory and call:
    docker build -t gitlab-registry.cern.ch/arnaez/opcualarlpgbtservertester/baseimageforquasar --platform linux/x86_64 .
    docker push gitlab-registry.cern.ch/arnaez/opcualarlpgbtservertester/baseimageforquasar
