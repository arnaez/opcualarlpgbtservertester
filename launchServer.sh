#!/bin/bash

echo "Setting environment variables"
export IPBUS_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-00-00/installed/external/x86_64-centos9-gcc11-opt/include/
export IPBUS_LIBRARY_PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-10-00-00/installed/external/x86_64-centos9-gcc11-opt/lib/
export LD_LIBRARY_PATH=${IPBUS_LIBRARY_PATH}:$LD_LIBRARY_PATH
LCG_version="LCG_102b_ATLAS_11"
PATH_TO_LCG=/cvmfs/sft.cern.ch/lcg/releases/
export BOOSTROOT=${PATH_TO_LCG}${LCG_version}/Boost/1.78.0/x86_64-centos9-gcc11-opt/
export LD_LIBRARY_PATH=${BOOST_PATH}/lib:$LD_LIBRARY_PATH
#echo "List of existing environment variables"
#env
echo "Checking if cvmfs mounts are there"
ls /cvmfs/
cd /ServerDir/
mv bin/* build/bin/
echo "Copying input connection file $1"
cp ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/hw-setup-configs/$1 /ServerDir/build/
echo "Copying input configuration file $2"
cp ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/hw-setup-configs/$2 /ServerDir/build/bin/config.xml
echo "Listing the servers' files"
find .
cd build
echo "Checking host info"
echo `hostname`
echo "Checking if port is available"
yum install -y lsof
lsof -i -P -n +c0 
echo "Checking running processes"
ps -aux
echo "Copying automatically generated python helper classes to client folder"
cp -R /ServerDir/PyUaoForQuasar ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/
echo "Launching the server's binaries"
echo "Start of log" > ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/logServer.txt
echo "Listing (again) the servers' files" &>> ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/logServer.txt
find /ServerDir &>> ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/logServer.txt
echo "Really starting..."
./bin/OpcUaLArLpGBTServer --opcua_backend_config ./bin/ServerConfig.xml --config_file ./bin/config.xml &>> ${CI_BUILDS_DIR}/${CI_PROJECT_NAME}/${CI_PIPELINE_ID}/logServer.txt
echo "Server's binaries exited."
